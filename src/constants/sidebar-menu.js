const sidebar_menu = [
  {
    id: 1,
    path: "/about",
    title: "About",
  },
  {
    id: 2,
    path: "/skills",
    title: "Skills",
  },
  {
    id: 3,
    path: "/resume",
    title: "Resume",
  },
  {
    id: 4,
    path: "/portfolio",
    title: "Portfolio",
  },
  {
    id: 5,
    path: "/contact",
    title: "Contact",
  },
];

export default sidebar_menu;
