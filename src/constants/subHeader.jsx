const subheaderContent = {
  about: {
    title: "WELCOME",
    content: "Thank you for your visit",
  },
  skills: {
    title: "Skills",
    content: "This is the Skills page subheader content.",
  },
  resume: {
    title: "Resume",
    content: "This is the Resume page subheader content.",
  },
  portfolio: {
    title: "Portfolio",
    content: "This is the Portfolio page subheader content.",
  },
  contact: {
    title: "Contact",
    content: "This is the Contact page subheader content.",
  },
};

export default subheaderContent;
