import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

import SideBarItem from "./sidebar-item";

import subheaderContent from "./../../constants/subHeader";

function SideBar({ menu }) {
  const location = useLocation();

  const [active, setActive] = useState(1);

  useEffect(() => {
    menu.forEach((element) => {
      if (location.pathname === element.path) {
        setActive(element.id);
      }
    });
  }, [location.pathname, menu]);

  const __navigate = (id) => {
    setActive(id);
  };

  return (
    <div>
      <nav>
        <div>
          <div className="w-full flex justify-center">
            <div className=" px-10 flex h-24 container items-center space-x-5 justify-start font-semibold">
              {menu.map((item, index) => (
                <div key={index} onClick={() => __navigate(item.id)}>
                  <SideBarItem active={item.id === active} item={item} />
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="w-full h-48 text-white bg-black">
          <div className=" flex justify-center items-center h-full">
            <div className="container space-y-10 px-10">
              <h2 className="font-bold text-5xl">
                {subheaderContent[menu[active - 1].path.slice(1)].title}
              </h2>
              <p className="text-3xl italic">
                {subheaderContent[menu[active - 1].path.slice(1)].content}
              </p>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default SideBar;
