import React, { useState } from "react";
import { Link } from "react-router-dom";

const SideBarItem = ({ item, active }) => {
  const [hover, setHover] = useState(false);

  const handleMouseEnter = () => {
    setHover(true);
  };

  const handleMouseLeave = () => {
    setHover(false);
  };

  return (
    <Link
      to={item.path}
      className={`sidebar-item ${active ? "sidebar-item-active" : ""}`}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <span
        className={`sidebar-item-label ${
          hover || active ? "border-b-2 pb-2 border-black" : "border-b-0 pb-0"
        }`}
      >
        {item.title}
      </span>
    </Link>
  );
};

export default SideBarItem;
