import React from "react";
import SideBar from "../Sidebar/SideBar";
import sidebar_menu from "../../constants/sidebar-menu";

export default function Header() {
  return (
    <div>
      <SideBar menu={sidebar_menu} />
    </div>
  );
}
