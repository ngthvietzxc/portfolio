import React from "react";

export default function About() {
  return (
    <div>
      <div className="flex justify-center ">
        <div
          className="container flex "
          style={{ fontFamily: "'PT Serif', serif" }}
        >
          <div className="w-1/2 px-10 break-words space-y-5">
            <div>
              <p className="font-normal text-lg">
                My name is Nguyễn Thành Việt, and I am a front-end developer
                living in Ho Chi Minh City. I have a great passion for creating
                beautiful, interactive, and efficient web applications. With my
                knowledge and skills, I strive to create smooth, user-friendly
                UIs that provide the best possible user experience.
              </p>
            </div>
            <div className="flex justify-between">
              <div>
                <h2 className="font-bold text-xl mb-4">Information</h2>
                <div className="flex flex-col">
                  <div className="flex items-center text-lg ">
                    <span className="font-bold w-20">Name:</span>
                    <p>Nguyễn Thành Việt</p>
                  </div>
                  <div className="flex items-center text-lg ">
                    <span className="font-bold w-20">Email:</span>
                    <p>ngthvietzxc@gmail.com</p>
                  </div>
                  <div className="flex items-center text-lg">
                    <span className="font-bold w-20">Phone:</span>
                    <p>0967054167</p>
                  </div>
                  <div className="flex items-center text-lg">
                    <span className="font-bold w-20">Address:</span>
                    <p>Phu Nhuan District, Ho Chi Minh City</p>
                  </div>
                  <div className="flex items-center text-lg">
                    <span className="font-bold w-20">Gitlab:</span>
                    <p>
                      <a
                        href="https://gitlab.com/ngthvietzxc"
                        className="text-blue-500 hover:underline"
                      >
                        https://gitlab.com/ngthvietzxc
                      </a>
                    </p>
                  </div>
                </div>
              </div>

              <div className="items-center flex pr-10 pt-5">
                <img
                  className=" w-32 h-32 rounded-full rotate-12"
                  src="https://scontent.fdad3-6.fna.fbcdn.net/v/t39.30808-6/338817433_575915677828080_1162505111050877449_n.jpg?_nc_cat=100&cb=99be929b-3346023f&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=DgIjVfo3tswAX8HB6au&_nc_ht=scontent.fdad3-6.fna&oh=00_AfCsSPFNSHxtigNMe3b2RfVu9CVDHRzdqQa7duJ0BhFk7g&oe=647A2186"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
