import React from "react";

export default function Skills() {
  return (
    <div className="flex justify-center">
      <div
        className="container flex text-lg"
        style={{ fontFamily: "'PT Serif', serif" }}
      >
        <div className="w-1/2 px-10 break-words">
          <div className="space-y-5">
            <div className="flex items-center text-lg">
              <span className="font-bold w-40">HTML/CSS</span>
              <p className="space-x-3">
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  HTML5
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  CSS3
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Bootstrap 4
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  CSS
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  SCSS/SASS
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Responsive
                </span>
              </p>
            </div>
            <div className="flex items-center text-lg">
              <span className="font-bold w-40">Javascript</span>
              <p className="space-x-3">
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  ES5/ES6
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Jquery
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Axios
                </span>
              </p>
            </div>
            <div className="flex items-center text-lg">
              <span className="font-bold w-40">ReactJS</span>
              <p className="space-x-3">
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  ReactJS basic
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Router
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Redux
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Hooks
                </span>
              </p>
            </div>
            <div className="flex items-center text-lg">
              <span className="font-bold w-40">Version Control</span>
              <p className="space-x-3">
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Git Basic
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  SVN
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  GitLab
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Github
                </span>
              </p>
            </div>
            <div className="flex items-center text-lg">
              <span className="font-bold w-40">Knowledge base</span>
              <p className="space-x-3">
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  OOP
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Data Structure and Algorithms
                </span>
              </p>
            </div>
            <div className="flex items-center text-lg">
              <span className="font-bold w-40">Other languages</span>
              <p className="space-x-3">
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Java basic
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  C basic
                </span>
              </p>
            </div>
            <div className="flex items-center text-lg">
              <span className="font-bold w-40">Soft skills</span>
              <p className="space-x-3">
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Teamwork
                </span>
                <span className="hover:text-white hover:bg-gray-700 border border-black p-1">
                  Self study
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
